# WOT tier matchmaking analyzer

As referenced on [this Reddit thread](https://www.reddit.com/r/WorldofTanks/comments/r4shpe/think_you_get_unfairly_treated_by_the_matchmaker/).

Simple enough Python script. It is lacking in error control and proper logging.
if you run into issues parsing some file, I suggest adding a printout,
`print(filename)`, below the first statement.

The details on how to decode the replay format comes from [this forum post](http://forum.worldoftanks.eu/index.php?/topic/105682-wot-replay-file-format/).
