#!/usr/bin/env python3

import io
import json
import pathlib
import struct
import sys

tier = {}
results = {}

# Read tanklist into tier dict for easy lookup.
tanklist = json.load(open('tanklist.json', 'r'))
for tank in tanklist['data']:
  tier[tanklist['data'][tank]['tag']] = tanklist['data'][tank]['tier']

for filename in list(pathlib.Path(sys.argv[1]).glob('*.wotreplay')):
  failures = []

  # open replay file
  file = open(filename, 'rb')

  # ignore first 8 bytes which is 4 bytes magic number (288633362 int32)
  # followed by 4 bytes of match data.
  file.seek(8)

  # read size of pre-battle JSON structure
  size = struct.unpack("<L", file.read(4))[0] # unpack binary into int32

  # read pre-battle JSON structure
  bdata = file.read(size) # read json data

  # we're not reading the post-battle results, so lets close it up for now.
  file.close()

  try:
    data = json.loads(bdata)
  except:
    # Couldn't decode JSON; should probably throw an error message here.
    continue

  # Extract game type
  game_type = data['gameplayID']
  # Extract my vehicle from pre-battle JSON
  # (need to reformat it slightly as WG doesn't grasp proper data management)
  my_vehicle = data['playerVehicle'].split('-', 1)[1]

  if my_vehicle in tier:
    my_tier = tier[my_vehicle]
  else:
    my_tier = 0
    failures.append(f'unknown_tier({my_vehicle:s})')

  # I blame this line on the alcohol.
  try:
    tiers = sorted(list(set([ tier[data['vehicles'][v]['vehicleType'].split(':', 1)[1]] for v in data['vehicles'] ])))
  except:
    tiers = []

  # sort game types, tier and matchmaking into bucket keys
  if len(tiers) == 1 and tiers[0] == my_tier:
    result = 'all-same-tier'
  elif len(tiers) == 2 and tiers[0] == my_tier:
    result = 'plus-one-bottom-tier'
  elif len(tiers) == 2 and tiers[1] == my_tier:
    result = 'plus-one-top-tier'
  elif len(tiers) == 3 and tiers[0] == my_tier:
    result = 'plus-two-bottom-tier'
  elif len(tiers) == 3 and tiers[1] == my_tier:
    result = 'plus-two-mid-tier'
  elif len(tiers) == 3 and tiers[2] == my_tier:
    result = 'plus-two-top-tier'
  else:
    result = 'unknown'

  # store results into buckets 
  failure_str = '/'.join(failures)
  result_key = f'{game_type:s},{my_tier:d},{result:s},{failure_str:s}'
  results[result_key] = results.get(result_key, 0) + 1

  # Also relevant information:
  #   premium account
  #   premium tank
  #   test server, training match, halloween games etc.
  # relevant, but needs more work:
  #   win/lose/draw/unknown
  #   survived/died
  #   kills
  #   damage
  #   base exp

  # This will print out an error message for all halloween games, training games etc.
  #if failure_str:
  #  print(f'Failed to parse {str(filename):s}: {failure_str:s}') # TODO logger

# Print all results in the bucket in CSV format.

print("#Mode,Tier,Tag,Errormessage,Count")
for r in sorted(list(set(results))):  
  print(f'{r:s},{results[r]:d}')
